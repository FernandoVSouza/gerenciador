import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

import { IAuth, IUserAutenticated } from '../../models/auth.model';
import { AuthService } from '../../services/auth.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  // cadastrando: boolean = false;
  auth: IAuth = { email: '', password: ''};

  constructor(
    private _authService: AuthService,
    private _router: Router
  ) { }

  ngOnInit() { }

  autenticateUser(auth: IAuth): void {    
    this._authService.autenticateUser(auth)
      .subscribe((ret) => {
        const user: IUserAutenticated = ret;
        this._authService.saveUserAutenticated(user);
        this._router.navigate(['/home']);
      },
      error => {
        console.error(`Erro: ${error}.`);
      });
  }

}