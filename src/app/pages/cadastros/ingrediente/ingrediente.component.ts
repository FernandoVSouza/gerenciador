import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { ToastrService } from 'ngx-toastr';

import { IIngrediente } from 'src/app/models/ingrediente.model';
import { IngredienteService } from '../../../services/ingrediente.service';

@Component({
  selector: 'app-ingrediente',
  templateUrl: './ingrediente.component.html',
  styleUrls: ['./ingrediente.component.css']
})
export class IngredienteComponent implements OnInit {
  p: number = 1;
  ingredientes: IIngrediente[] = null;
  ingredienteForm: FormGroup = new FormGroup({
    id_ingrediente: new FormControl(''),
    descricao: new FormControl('', [ Validators.required, Validators.maxLength(50)]),
    incremento: new FormControl(null, [ Validators.required]),
    preco: new FormControl(null),
    ativo: new FormControl(null),
  });

  constructor(
    private ingredienteService: IngredienteService,
    private _toastService: ToastrService
  ) { }

  ngOnInit() {
    this.carregarIngredientes();
  }

  //EVENTS

  submitIngrediente(): void {
    const ingrediente: IIngrediente = this.getIngredienteInfoFromFormCadastro();
    if(!ingrediente.id_ingrediente) {
      this.ingredienteService.cadastrarIngrediente(ingrediente)
        .subscribe(result => {
          this._toastService.success('Produto inserido com sucesso!', 'Sucesso!');
          this.carregarIngredientes();
          this.refreshForm();
        }, error => {
          this._toastService.error('Ocorreu um erro!', 'Erro:');
          console.error(error);
        });
    } else {
      this.ingredienteService.updateIngrediente(ingrediente)
        .subscribe(result => {
          this._toastService.success('Produto alterado com sucesso!', 'Sucesso!');
          this.carregarIngredientes();
          this.refreshForm();
        }, error => {
          this._toastService.error('Ocorreu um erro!', 'Erro:');
          console.error(error);
        });
    }
  }

  refreshForm() {
    this.ingredienteForm.reset();
  }

  async changeStatusIngrediente(ingrediente: IIngrediente) {
    try {
      if(ingrediente.ativo) {
        await this.ingredienteService.desativarIngrediente(ingrediente);
        this._toastService.success('Desativo com sucesso!', 'Sucesso:');
      } else {
        await this.ingredienteService.ativarIngrediente(ingrediente);
        this._toastService.success('Ativado com sucesso!', 'Sucesso:');
      }
      this.carregarIngredientes();
    } catch (error) {
      this._toastService.error('Ocorreu um erro!', 'Erro:');
      console.error(error);
    }
  }

  insertIngredienteInfoOnFormCadastro(ingrediente: IIngrediente): void {
    this.ingredienteForm.patchValue({
      id_ingrediente: ingrediente.id_ingrediente,
      descricao: ingrediente.descricao,
      incremento: ingrediente.incremento,
      preco: ingrediente.preco,
      ativo: ingrediente.ativo
    });
  }

  //METHODS

  carregarIngredientes(): void {
    this.ingredienteService.getIngredientes()
      .subscribe((ingredientes: IIngrediente[]) => {
        this.ingredientes = ingredientes;
      }, error => {
        this._toastService.error('Ocorreu um erro ao carregar os ingredientes!', 'Erro:');
        console.error(error);
      });
  }

  getIngredienteInfoFromFormCadastro(): IIngrediente {
    return {
      id_ingrediente: this.ingredienteForm.value.id_ingrediente,
      descricao: this.ingredienteForm.value.descricao,
      preco: this.ingredienteForm.value.preco,
      incremento: this.ingredienteForm.value.incremento,
      ativo: this.ingredienteForm.value.ativo,
    };
  }

}