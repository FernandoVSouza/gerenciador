import { IngredienteModule } from './ingrediente.module';

describe('IngredienteModule', () => {
  let ingredienteModule: IngredienteModule;

  beforeEach(() => {
    ingredienteModule = new IngredienteModule();
  });

  it('should create an instance', () => {
    expect(ingredienteModule).toBeTruthy();
  });
});
