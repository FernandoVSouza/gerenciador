import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { ReactiveFormsModule } from '@angular/forms';
import { NgxPaginationModule } from 'ngx-pagination';

import { IngredienteRoutingModule } from './ingrediente-routing.module';
import { IngredienteComponent } from './ingrediente.component';
import { CurrencyMaskModule } from "ng2-currency-mask";

@NgModule({
  imports: [
    CommonModule,
    IngredienteRoutingModule,
    FormsModule,
    ReactiveFormsModule,
    NgxPaginationModule,
    CurrencyMaskModule
  ],
  declarations: [IngredienteComponent],
  providers: []
})
export class IngredienteModule { }
