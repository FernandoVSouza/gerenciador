import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { ToastrService } from 'ngx-toastr';

import { ICategoria } from 'src/app/models/categoria.model';
import { CategoriaService } from '../../../services/categoria.service';

@Component({
  selector: 'app-categoria',
  templateUrl: './categoria.component.html',
  styleUrls: ['./categoria.component.css']
})
export class CategoriaComponent implements OnInit {
  p: number = 1;
  pModal: number = 1;
  categorias: ICategoria[] = null;
  categoriaForm: FormGroup = new FormGroup({
    id_categoria: new FormControl(''),
    descricao: new FormControl('', [ Validators.required, Validators.maxLength(50)]),
    alteracao: new FormControl(null, [ Validators.required]),
    incremento: new FormControl(null, [ Validators.required]),
    ordem: new FormControl(null, [ Validators.required]),
    ativo: new FormControl(null),
  });

  constructor(
    private categoriaService: CategoriaService,
    private _toastService: ToastrService
  ) { }

  ngOnInit() {
    this.carregarCategorias();
  }

  //EVENTS

  submitCategoria(): void {
    const categoria: ICategoria = this.getCategoriaInfoFromFormCadastro();

    if(!categoria.id_categoria) {
      this.categoriaService.cadastrarCategoria(categoria)
        .subscribe(result => {
          this._toastService.success('Produto inserido com sucesso!', 'Sucesso!');
          this.carregarCategorias();
          this.refreshForm();
        }, error => {
          this._toastService.error('Ocorreu um erro!', 'Erro:');
          console.error(error);
        });
    } else {
      this.categoriaService.updateCategoria(categoria)
        .subscribe(result => {
          this._toastService.success('Produto alterado com sucesso!', 'Sucesso!');
          this.carregarCategorias();
          this.refreshForm();
        }, error => {
          this._toastService.error('Ocorreu um erro!', 'Erro:');
          console.error(error);
        });
    }
  }

  refreshForm() {
    this.categoriaForm.reset();
  }

  async changeStatusCategoria(categoria: ICategoria) {
    try {
      if(categoria.ativo) {
        await this.categoriaService.desativarCategoria(categoria);
        this._toastService.success('Desativo com sucesso!', 'Sucesso:');
      } else {
        await this.categoriaService.ativarCategoria(categoria);
        this._toastService.success('Ativado com sucesso!', 'Sucesso:');
      }
      this.carregarCategorias();
    } catch (error) {
      this._toastService.error('Ocorreu um erro!', 'Erro:');
      console.error(error);
    }
  }

  insertCategoriaInfoOnFormCadastro(categoria: ICategoria): void {
    this.categoriaForm.patchValue({
      id_categoria: categoria.id_categoria,
      descricao: categoria.descricao,
      alteracao: categoria.alteracao,
      incremento: categoria.incremento,
      ordem: categoria.ordem,
      ativo: categoria.ativo
    });
  }

  //METHODS

  carregarCategorias(): void {
    this.categoriaService.getCategorias()
      .subscribe((categorias: ICategoria[]) => {
        this.categorias = categorias;
      }, error => {
        this._toastService.error('Ocorreu um erro ao carregar as categorias!', 'Erro:' + error);
        console.error(error);
      });
  }

  getCategoriaInfoFromFormCadastro(): ICategoria {
    return {
      id_categoria: this.categoriaForm.value.id_categoria,
      descricao: this.categoriaForm.value.descricao,
      alteracao: this.categoriaForm.value.alteracao,
      incremento: this.categoriaForm.value.incremento,
      ordem: this.categoriaForm.value.ordem,
      ativo: this.categoriaForm.value.ativo,
    };
  }

}