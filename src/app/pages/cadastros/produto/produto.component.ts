import { Component, OnInit, TemplateRef } from '@angular/core';
import { BsModalService } from 'ngx-bootstrap/modal';
import { BsModalRef } from 'ngx-bootstrap/modal/bs-modal-ref.service';
import { ToastrService } from 'ngx-toastr';
import { FormGroup, FormControl, Validators } from '@angular/forms';

import { IngredienteProduto } from 'src/app/models/ingrediente.model';
import { IngredienteService } from '../../../services/ingrediente.service';
import { ProdutoService } from '../../../services/produto.service';
import { IProduto, IProdutoModel } from '../../../models/produto.model';
import { CategoriaService } from '../../../services/categoria.service';
import { ICategoria } from '../../../models/categoria.model';

@Component({
  selector: 'app-produto',
  templateUrl: './produto.component.html',
  styleUrls: ['./produto.component.css']
})
export class ProdutoComponent implements OnInit {
  p: number = 1;
  modalRef: BsModalRef;
  produtos: IProduto[] = null;
  ingredientes: IngredienteProduto[] = null;
  categorias: ICategoria[] = null;
  alterarProduto: boolean = false;

  produtoForm: FormGroup = new FormGroup({
    id_produto: new FormControl(''),
    descricao: new FormControl('', [ Validators.required, Validators.maxLength(25)]),
    preco_unitario: new FormControl(null, [ Validators.required]),
    fk_categoria: new FormControl(null, [ Validators.required]),
    ativo: new FormControl(null),
  });

  constructor(
    private produtoService: ProdutoService,
    private ingredienteService: IngredienteService,
    private categoriaService: CategoriaService,
    private modalService: BsModalService,
    private _toastService: ToastrService
  ) { }

  ngOnInit() {
    this.carregarCategorias();
    this.carregarIngredientes();
    this.carregarProdutos();
  }

  //EVENTS

  openIngredienteModal(ingredienteModal: TemplateRef<any>) {
    this.modalRef = this.modalService.show(ingredienteModal);
  }

  submitProduto(): void {
    const produto: IProduto = this.getProdutoInfoFromFormCadastro();

    if(!produto.id_produto) {
      this.produtoService.cadastrarProduto(produto)
        .subscribe(result => {
          this._toastService.success('Produto inserido com sucesso!', 'Sucesso!');
          this.carregarProdutos();
          this.refreshForm();
        }, error => {
          this._toastService.error(error.error, 'Erro:');
          console.error(error);
        });
    } else {
      this.produtoService.updateProduto(produto)
        .subscribe(() => {
          this.carregarProdutos();
          this.refreshForm();
          this._toastService.success('Produto alterado com sucesso!', 'Sucesso!');
        }, error => {
          this._toastService.error('Ocorreu um erro!', 'Erro:');
          console.error(error);
        });
    }
  }

  refreshForm() {
    this.produtoForm.reset();
    this.carregarIngredientes();
    this.alterarProduto = false;
  }

  async changeStatusProduto(produto: IProduto) {
    try {
      if(produto.ativo) {
        await this.produtoService.desativarProduto(produto);
        this._toastService.success('Desativo com sucesso!', 'Sucesso:');
      } else {
        await this.produtoService.ativarProduto(produto);
        this._toastService.success('Ativado com sucesso!', 'Sucesso:');
      }
      this.carregarProdutos();
    } catch (error) {
      this._toastService.error('Ocorreu um erro!', 'Erro:');
      console.error(error);
    }
  }

  insertProdutoInfoOnFormCadastro(produto: IProduto): void {
    this.produtoForm.patchValue({
      id_produto: produto.id_produto,
      descricao:  produto.descricao,
      fk_categoria:  produto.fk_categoria,
      preco_unitario:  produto.preco_unitario,
      ativo:  produto.ativo,
    });

    this.ingredientes.map(i => {
      i.checked = false;
    });

    this.ingredientes.map(i => {
      produto.ingredientes.map(pin => {
        if(pin == i.id_ingrediente) {
          i.checked = !i.checked;
        }
      });
    });
    this.verficaCategoriaPermiteIncremento();
  }

  confirmaIngredientes() {
    this.modalRef.hide();
  }

  async changeStatusProdutos(produto: IProduto) {
    try {
      if(produto.ativo) {
        await this.produtoService.desativarProduto(produto);
      } else {
        await this.produtoService.ativarProduto(produto);
      }
      this.carregarProdutos();
    } catch (error) {
      this._toastService.error('Ocorreu um erro!', 'Erro: ' + error);
      console.error(error);
    }
  }

  verficaCategoriaPermiteIncremento(): void {
    const fk_categoria = this.produtoForm.value.fk_categoria;
    const categoria = this.categorias.find(c => c.id_categoria == fk_categoria );
    if(!categoria.incremento) {
      this.alterarProduto = true;
      this.ingredientes.forEach(i => {
        if(i.checked == true) {
          i.checked = false;
        }
      });
    } else {
      this.alterarProduto = false;
    }
  }

  async changeDestaque(produto: IProdutoModel) {
    try {
      await this.produtoService.alterDestaque(produto.id_produto);
        if(produto.destaque) {
          this._toastService.success('Produto removido aos Destaques com sucesso!');
        } else {
          this._toastService.success('Produto adicionado aos Destaques com sucesso!');
        }
      this.carregarProdutos();
    } catch (error) {
      this._toastService.error('Ocorreu um erro!', 'Erro: ' + error);
      console.error(error);
      this.carregarProdutos();
    }
  }

  //METHODS

  carregarProdutos(): void {
    this.produtoService.getProdutos()
      .subscribe((produtos: IProdutoModel[]) => {
        this.produtos = produtos;
      }, error => {
        this._toastService.error('Ocorreu um erro ao carregar os produtos!', 'Erro:');
        console.error(error);
      });
  }

  carregarIngredientes(): void {
    this.ingredientes = [];
    this.ingredienteService.getIngredientes()
      .subscribe((ingredientes: IngredienteProduto[]) => {
        this.ingredientes  = ingredientes.filter( i => {
          if (i.ativo == true ) { //&& !i.incremento
            i.checked = false;
            return i;
          }
        });
      }, error => {
        this._toastService.error('Ocorreu um erro ao carregar os ingredientes!', 'Erro:');
        console.error(error);
      });
  }

  carregarCategorias(): void {
    this.categoriaService.getCategorias()
      .subscribe((categorias: ICategoria[]) => {
        this.categorias = categorias;
      }, error => {
        this._toastService.error('Ocorreu um erro ao carregar as categorias!', 'Erro:');
        console.error(error);
      });
  }

  getProdutoInfoFromFormCadastro(): IProduto {
    let ingredientes: number[] = [];
    this.ingredientes.forEach(i => {
      if(i.checked == true) {
        ingredientes.push(i.id_ingrediente);
      }
    });

    return {
      id_produto: this.produtoForm.value.id_produto,
      descricao: this.produtoForm.value.descricao,
      fk_categoria: this.produtoForm.value.fk_categoria,
      preco_unitario: this.produtoForm.value.preco_unitario,
      ingredientes: ingredientes,
      ativo: this.produtoForm.value.ativo,
    };
  }

}
