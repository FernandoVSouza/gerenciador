import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { ReactiveFormsModule } from '@angular/forms';
import { NgxPaginationModule } from 'ngx-pagination';
import { CurrencyMaskModule } from "ng2-currency-mask";

import { AppBootstrapModule } from '../../../app-bootstrap/app-bootstrap.module';
import { ProdutoRoutingModule } from './produto-routing.module';
import { ProdutoComponent } from './produto.component';

@NgModule({
  imports: [
    CommonModule,
    AppBootstrapModule,
    ProdutoRoutingModule,
    FormsModule,
    ReactiveFormsModule,
    NgxPaginationModule,
    CurrencyMaskModule
  ],
  declarations: [ProdutoComponent]
})
export class ProdutoModule { }
