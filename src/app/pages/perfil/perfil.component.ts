import { Component, OnInit } from '@angular/core';
import { ToastrService } from 'ngx-toastr';

import { AuthService } from '../../services/auth.service';
import { IUserAutenticated, IUser } from '../../models/auth.model';

@Component({
  selector: 'app-perfil',
  templateUrl: './perfil.component.html',
  styleUrls: ['./perfil.component.css']
})
export class PerfilComponent implements OnInit {
  userAuth: IUserAutenticated;
  user: IUser = { id: null, name: '', email: '', password: '',  cpassword: '', phone: ''};

  constructor(
    private _authService: AuthService,
    private _toastService: ToastrService
  ) {}

  async ngOnInit() {
    this.userAuth = await this._authService.getUserAutenticated();
    this.user.id = this.userAuth.user.id;
    this.user.name = this.userAuth.user.name;
    this.user.email = this.userAuth.user.email;
    this.user.phone = this.userAuth.user.phone;
  }

  //EVENTS

  updateUser(): void {
    console.log(this.user);
    // console.log(this.userAuth);

    this._authService.updateUser(this.user, this.user.id)
      .subscribe(
        retorno => {
          console.log(retorno);
          this.userAuth.user = this.user;
          this._authService.saveUserAutenticated(this.userAuth);
          this._toastService.success('Alterado com sucesso!');
        }, error => {
          console.error(error);
          this._toastService.error('Ops! Ocorreu um erro ao alterar');
        }
      )
    }
}
