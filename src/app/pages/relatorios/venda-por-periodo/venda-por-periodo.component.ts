import { Component, OnInit } from '@angular/core';

import { ToastrService } from 'ngx-toastr';
import { RelatorioService } from '../../../services/relatorios.service';
import { Chart } from 'chart.js';

@Component({
  selector: 'app-venda-por-periodo',
  templateUrl: './venda-por-periodo.component.html',
  styleUrls: ['./venda-por-periodo.component.css']
})
export class VendaPorPeriodoComponent implements OnInit {
  relatorio = { dataInicial: null, dataFinal: null };
  dataTeste: string = '';
  chart = [];

  constructor(
    private _toastService: ToastrService,
    private _relatorioService: RelatorioService
  ) { }

  ngOnInit() {}

  btnPesquisarClick(): void {
    this._getRelatorioVendaPorPeriodo(this.relatorio)
  }

  refreshForm(): void {
    this.relatorio = { dataInicial: null, dataFinal: null };
  }

  private _getRelatorioVendaPorPeriodo({ dataInicial, dataFinal }): void {
    this._relatorioService.getRelatorioVendaPorPeriodo(dataInicial, dataFinal)
      .subscribe(
        retorno => {
          this._configureChart(retorno);
        }, error => {
          console.error(error);
          this._toastService.error('Ops. Erro ao retornar os dados!');
        }
      );
  }

  private _configureChart(dados): void {
    const labelEValores = this._getLabel(dados);

    this.chart = new Chart('canvas', {
      type: 'bar',
      data: {
        labels: labelEValores.label,
        datasets: [
          {
            label: 'Vendas no Período',
            data: labelEValores.valores,
            backgroundColor: "#4DC0B5",
            fill: false
          }
        ]
      },
      options: {
        legend: {
          display: true
        },
        scales: {
          xAxes: [{
            stacked: true
          }],
          yAxes: [{
            stacked: true
          }],
        }
      }
    });
  }

  private _getLabel(data: any[]): { label: any[], valores: any[] } {
    let label = [];
    let valores = [];

    data.forEach(d => {
      label.push(d.data);
      valores.push(Number(d.valor_total));
    });

    return { label: label,  valores: valores};
  }

}
