import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { VendaPorPeriodoComponent } from './venda-por-periodo.component';

const routes: Routes = [
  {
    path: '',
    component: VendaPorPeriodoComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class VendaPorPeriodoRoutingModule { }
