import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { VendaPorPeriodoRoutingModule } from './venda-por-periodo-routing.module';
import { VendaPorPeriodoComponent } from './venda-por-periodo.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NgxPaginationModule } from 'ngx-pagination';

@NgModule({
  imports: [
    CommonModule,
    VendaPorPeriodoRoutingModule,
    FormsModule,
    ReactiveFormsModule,
    NgxPaginationModule
  ],
  declarations: [VendaPorPeriodoComponent]
})
export class VendaPorPeriodoModule { }
