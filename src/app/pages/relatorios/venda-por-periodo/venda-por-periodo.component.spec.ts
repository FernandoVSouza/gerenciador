import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { VendaPorPeriodoComponent } from './venda-por-periodo.component';

describe('VendaPorPeriodoComponent', () => {
  let component: VendaPorPeriodoComponent;
  let fixture: ComponentFixture<VendaPorPeriodoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ VendaPorPeriodoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(VendaPorPeriodoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
