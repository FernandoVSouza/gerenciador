import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { VendaPorPeriodoRoutingModule } from './cardapio-routing.module';
import { CardapioComponent } from './cardapio.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NgxPaginationModule } from 'ngx-pagination';

@NgModule({
  imports: [
    CommonModule,
    VendaPorPeriodoRoutingModule,
    FormsModule,
    ReactiveFormsModule,
    NgxPaginationModule
  ],
  declarations: [CardapioComponent]
})
export class CardapioModule { }
