import { Component, OnInit } from '@angular/core';

import { ToastrService } from 'ngx-toastr';
import { RelatorioService } from '../../../services/relatorios.service';

@Component({
  selector: 'app-cardapio',
  templateUrl: './cardapio.component.html',
  styleUrls: ['./cardapio.component.css']
})
export class CardapioComponent implements OnInit {
  cardapio: any[] = null;
  collapsed1 = false;
  collapsed2 = false;

  constructor(
    private _toastService: ToastrService,
    private _relatorioService: RelatorioService
  ) { }

  ngOnInit() {
    this._getCardapio();
  }

  btnCollapseClick(categoria: any): void {
    this.cardapio.forEach(r => {
      if(r.id_categoria == categoria.id_categoria) {
        r.collapsed = !r.collapsed;
      } else {
        r.collapsed = false;
      }
    });
  }

  private _getCardapio(): void {
    this._relatorioService.getCardapio()
      .subscribe(
        retorno => {
          retorno.forEach(r => {
            r.collapsed = false;
            r.produtos.forEach(p => {
              p.ingredientes = p.ingredientes.join(', ');
            });
          });
          this.cardapio = retorno;
        }, error => {
          console.error(error);
          this._toastService.error('Ops. Erro ao retornar os dados!');
        }
      );
  }
}