import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PedidoPorPeriodoComponent } from './pedido-por-periodo.component';

describe('PedidoPorPeriodoComponent', () => {
  let component: PedidoPorPeriodoComponent;
  let fixture: ComponentFixture<PedidoPorPeriodoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PedidoPorPeriodoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PedidoPorPeriodoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
