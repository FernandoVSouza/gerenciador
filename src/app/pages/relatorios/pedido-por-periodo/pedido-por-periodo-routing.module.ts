import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { PedidoPorPeriodoComponent } from './pedido-por-periodo.component';

const routes: Routes = [
  {
    path: '',
    component: PedidoPorPeriodoComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class PedidoPorPeriodoRoutingModule { }
