import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { PedidoPorPeriodoRoutingModule } from './pedido-por-periodo-routing.module';
import { PedidoPorPeriodoComponent } from './pedido-por-periodo.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NgxPaginationModule } from 'ngx-pagination';
import { NgxMaskModule } from 'ngx-mask'

@NgModule({
  imports: [
    CommonModule,
    PedidoPorPeriodoRoutingModule,
    FormsModule,
    ReactiveFormsModule,
    NgxPaginationModule,
    NgxMaskModule.forRoot()
  ],
  declarations: [PedidoPorPeriodoComponent]
})
export class PedidoPorPeriodoModule { }
