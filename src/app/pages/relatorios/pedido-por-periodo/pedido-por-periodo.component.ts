import { Component, OnInit } from '@angular/core';

import { ToastrService } from 'ngx-toastr';
import { RelatorioService } from '../../../services/relatorios.service';
import { Chart } from 'chart.js';

@Component({
  selector: 'app-pedido-por-periodo',
  templateUrl: './pedido-por-periodo.component.html',
  styleUrls: ['./pedido-por-periodo.component.css']
})
export class PedidoPorPeriodoComponent implements OnInit {
  relatorio = { dataInicial: null, dataFinal: null };
  chartP = [];
  chartS = [];

  constructor(
    private _toastService: ToastrService,
    private _relatorioService: RelatorioService
  ) { }

  ngOnInit() {}

  btnPesquisarClick(): void {
    try {
      this._getRelatorioPedidoPorPeriodo(this.relatorio);
    } catch (error) {
      this._toastService.error('Erro: ' + error);
    }
  }

  refreshForm(): void {
    this.relatorio = { dataInicial: null, dataFinal: null };
  }

  private _getRelatorioPedidoPorPeriodo({ dataInicial, dataFinal }): void {
    this._relatorioService.getRelatorioPedidoPorPeriodo(dataInicial, dataFinal)
      .subscribe(
        retorno => {
          this._configureChart(retorno);
        }, error => {
          console.error(error);
          this._toastService.error('Ops. Erro ao retornar os dados!');
        }
      );
  }

  private _configureChart(dados): void {
    const primeiroPeriodo = this._getLabel(dados.primeiroPeriodo);
    const segundoPeriodo = this._getLabel(dados.segundoPeriodo);

    this.chartP = new Chart('canvasP', {
      type: 'bar',
      data: {
        labels: primeiroPeriodo.label,
        datasets: [{
          label: 'Período Informado',
          data: primeiroPeriodo.valores,
          backgroundColor: "rgba(255, 133, 43, 0.6)"
        }]
      },
      options: {
        legend: {
          display: true
        },
        scales: {
          xAxes: [{
            stacked: true
          }],
          yAxes: [{
            stacked: true
          }],
        }
      }
    });

    this.chartS = new Chart('canvasS', {
      type: 'bar',
      data: {
        labels: segundoPeriodo.label,
        datasets: [{
          label: 'Período Anterior',
          data: segundoPeriodo.valores,
          backgroundColor: "#1E90FF"
        }],
      },
      options: {
        legend: {
          display: true
        },
        scales: {
          xAxes: [{
            stacked: true
          }],
          yAxes: [{
            stacked: true
          }],
        }
      }
    });
  }

  private _getLabel(data: any[]): { label: any[], valores: any[] } {
    let label = [];
    let valores = [];

    data.forEach(d => {
      label.push(d.data);
      valores.push(Number(d.qtde_pedido));
    });

    return { label: label,  valores: valores};
  }

}
