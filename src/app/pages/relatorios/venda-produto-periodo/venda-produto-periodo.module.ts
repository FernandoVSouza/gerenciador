import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { VendaPorPeriodoRoutingModule } from './venda-produto-periodo-routing.module';
import { VendaPorProdutoPeriodoComponent } from './venda-produto-periodo.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NgxPaginationModule } from 'ngx-pagination';

@NgModule({
  imports: [
    CommonModule,
    VendaPorPeriodoRoutingModule,
    FormsModule,
    ReactiveFormsModule,
    NgxPaginationModule
  ],
  declarations: [VendaPorProdutoPeriodoComponent]
})
export class VendaPorProdutoPeriodoModule { }
