import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { VendaPorProdutoPeriodoComponent } from './venda-produto-periodo.component';

describe('VendaPorPeriodoComponent', () => {
  let component: VendaPorProdutoPeriodoComponent;
  let fixture: ComponentFixture<VendaPorProdutoPeriodoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ VendaPorProdutoPeriodoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(VendaPorProdutoPeriodoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
