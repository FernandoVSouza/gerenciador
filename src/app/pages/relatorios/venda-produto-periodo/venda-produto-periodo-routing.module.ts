import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { VendaPorProdutoPeriodoComponent } from './venda-produto-periodo.component';

const routes: Routes = [
  {
    path: '',
    component: VendaPorProdutoPeriodoComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class VendaPorPeriodoRoutingModule { }
