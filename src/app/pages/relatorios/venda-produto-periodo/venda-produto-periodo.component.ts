import { Component, OnInit } from '@angular/core';

import { ToastrService } from 'ngx-toastr';
import { RelatorioService } from '../../../services/relatorios.service';
import { CategoriaService } from '../../../services/categoria.service';
import { ICategoria } from '../../../models/categoria.model';

@Component({
  selector: 'app-venda-produto-periodo',
  templateUrl: './venda-produto-periodo.component.html',
  styleUrls: ['./venda-produto-periodo.component.css']
})
export class VendaPorProdutoPeriodoComponent implements OnInit {
  relatorio = { dataInicial: null, dataFinal: null, fk_categoria: 0, quantidade: 0 };
  categorias: ICategoria[] = null;
  produtos: any[];

  constructor(
    private _toastService: ToastrService,
    private _relatorioService: RelatorioService,
    private categoriaService: CategoriaService
  ) { }

  ngOnInit() {
    this._carregarCategorias();
  }

  btnPesquisarClick(): void {
    if(this.relatorio.fk_categoria != 0) this.relatorio.quantidade == 0;
    this._getRelatorioVendaPorPeriodo(this.relatorio);
  }

  refreshForm(): void {
    this.relatorio = { dataInicial: null, dataFinal: null, fk_categoria: 0, quantidade: 0 };
  }

  zeraQuantidade(): void {
    this.relatorio.quantidade = 0;
  }

  private _getRelatorioVendaPorPeriodo({ dataInicial, dataFinal, fk_categoria, quantidade }): void {
    this._relatorioService.getRelatorioVendaDeProdutoPorPeriodo(dataInicial, dataFinal, fk_categoria, quantidade)
      .subscribe(
        retorno => {
          this.produtos = retorno;
        }, error => {
          console.error(error);
          this._toastService.error('Ops. Erro ao retornar os dados!');
        }
      );
  }

  private _carregarCategorias(): void {
    this.categoriaService.getCategorias()
      .subscribe((categorias: ICategoria[]) => {
        this.categorias = categorias;
      }, error => {
        this._toastService.error('Ocorreu um erro ao carregar as categorias!', 'Erro:');
        console.error(error);
        this._toastService.error('Ops. Erro ao carregar as categorias!');
      });
  }

}
