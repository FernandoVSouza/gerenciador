import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { ConfiguracoesRoutingModule } from './configuracoes-routing.module';
import { ConfiguracoesComponent } from './configuracoes.component';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    ConfiguracoesRoutingModule
  ],
  declarations: [ConfiguracoesComponent]
})
export class ConfiguracoesModule { }
