import { Component, OnInit } from '@angular/core';

import { IConfiguracao } from 'src/app/models/configuracao.model';
import { ConfiguracaoService } from 'src/app/services/configuracao.service';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-configuracoes',
  templateUrl: './configuracoes.component.html',
  styleUrls: ['./configuracoes.component.css']
})
export class ConfiguracoesComponent implements OnInit {
  habilita: boolean = false;
  location: any;
  configuracao: IConfiguracao;

  constructor(
    private _configuracaoService: ConfiguracaoService,
    private _toastService: ToastrService
  ) {
    this.configuracao = this._initConfiguracao();
  }

  ngOnInit() {
    this._configuracaoService.getConfiguracao()
      .subscribe(
        retorno => {
          if(retorno) {
            this.configuracao = retorno;
          } else {
            this.configuracao = this._initConfiguracao();
          }
          this.habilita = true;
        },
        error => {
          this._toastService.error('Ocorreu um erro ao carregar as configurações!', 'Erro:' + error);
          this.habilita = false;
        }
      );

      if(navigator.geolocation){
        navigator.geolocation.getCurrentPosition(position => {
          this.location = position.coords;
        });
     }
  }

  btnSalvarConfiguracoesClick(): void {
    try {
      this._verificaConfiguracao();
      if(this.configuracao.id_configuracao) {
        this._configuracaoService.updateConfiguracao(this.configuracao)
        .subscribe(
          () => {
            this._toastService.success('Atualizado com sucesso!');
          },
          error => {
            this._toastService.error('Ocorreu um erro ao atualizar as configurações!', 'Erro:' + error);
          }
        );
      } else {
        this._configuracaoService.saveConfiguracao(this.configuracao)
        .subscribe(
          () => {
            this._toastService.success('Salvo com sucesso!');
          },
          error => {
            this._toastService.error('Ocorreu um erro ao atualizar as configurações!', 'Erro:' + error);
          }
        );
      }
    } catch (error) {
      this._toastService.error('Ocorreu um erro ao salvar as configurações!', 'Erro: ' + error);
    }
  }

  private _verificaConfiguracao(): void {
    if(this.configuracao == null) {
      throw 'Nenhum dado informado';
    } else if(!this.configuracao.status) {
      throw 'Informar se lanche está Aberto/Fechado';
    } else if(!this.configuracao.latitude) {
      throw 'Informar latitude';
    } else if(!this.configuracao.longitude) {
      throw 'Informar longitude';
    } else if(!this.configuracao.distancia_sem_taxa) {
      throw 'Informar distância sem taxa';
    } else if(this.configuracao.distancia_com_taxa && !this.configuracao.preco_entrega) {
      throw 'Necessário informar o preço da entrega caso a distância com a taxa seja informada';
    }
  }

  private _initConfiguracao(): IConfiguracao {
    return {
      status: 0,
      distancia_com_taxa: null,
      distancia_sem_taxa: null,
      id_configuracao: null,
      latitude: null,
      longitude: null,
      preco_entrega: null,
      ativo: null
    }
  }

}
