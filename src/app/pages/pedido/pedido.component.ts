import { Component, OnInit, TemplateRef } from '@angular/core';

import { PedidoService } from '../../services/pedido.service';
import { BsModalRef } from 'ngx-bootstrap/modal/bs-modal-ref.service';
import { BsModalService } from 'ngx-bootstrap/modal';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-pedido',
  templateUrl: './pedido.component.html',
  styleUrls: ['./pedido.component.css']
})
export class PedidoComponent implements OnInit {
  pedidos: any[];
  modalRef: BsModalRef;
  pedido: any;
  pausado: boolean = false;

  constructor(
    private _pedidoService: PedidoService,
    private _modalService: BsModalService,
    private _toastService: ToastrService
  ) { }

  ngOnInit() {
    this._getPedidosFromWeb();
    setInterval(()=> {
        this._getPedidosFromWeb();
    }, 30000);
  }

  btnDetalhesPedido(pedido: any, modal: TemplateRef<any>): void {
    this.pedido = pedido;
    this.modalRef = this._modalService.show(modal);
  }

  btnAceitarPedido(pedido: any): void {
    this._alteraStatusPedido(pedido.id_pedido, 'Preparando');
  }

  btnRecusarPedido(pedido: any): void {
    this._alteraStatusPedido(pedido.id_pedido, 'Recusado');
  }

  private _getPedidosFromWeb(): void {
    this._pedidoService.getPedidosAguardando()
    .subscribe(
      retorno => {
        this.pedidos = retorno;
      },
      error => {
        console.error(error);
        this._toastService.error('Erro ao carregar os Pedidos!');
      }
    );
  }

  private _alteraStatusPedido(id_pedido: number, status: string) {
    this._pedidoService.getAlteraStatusPedido(id_pedido, status)
      .subscribe(
        () => {
          if(status == 'Preparando') {
            this._toastService.success('Pedido aceito com sucesso!')
          } else {
            this._toastService.success('Pedido recusado com sucesso!')
          }
          this._getPedidosFromWeb();
        }, error => {
          this._toastService.error('Ocorreu um erro ao Recusar o Pedido');
        }
      );
  }
}
