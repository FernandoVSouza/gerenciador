import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { PedidoRoutingModule } from './pedido-routing.module';
import { PedidoComponent } from './pedido.component';
import { AppBootstrapModule } from '../../app-bootstrap/app-bootstrap.module';
import { ReactiveFormsModule } from '@angular/forms';

@NgModule({
  imports: [
    CommonModule,
    PedidoRoutingModule,
    AppBootstrapModule,
    ReactiveFormsModule
  ],
  declarations: [PedidoComponent]
})
export class PedidoModule { }
