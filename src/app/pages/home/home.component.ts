import { Component, OnInit } from '@angular/core';

import { DashboardService } from '../../services/dashboard.service';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {
  dashboard: any;

  constructor(
    private _dashboardService: DashboardService,
    private _toastService: ToastrService
  ) { }

  ngOnInit() {
    try {
      this._getDashboard();
    } catch (error) {
      this._toastService.error(`Ops! Ocorreu um erro ao carregar o Dashboard!`);
    }
  }

  private _getDashboard(): void {
    this._dashboardService.getDashboard()
    .subscribe(
      retorno => {
        this.dashboard = retorno;
      },
      error => {
        console.error(`Erro: ${error}`);
        throw error;
      }
    );
  }

}
