import { HistoricoPedidoModule } from './historico-pedido.module';

describe('HistoricoPedidoModule', () => {
  let historicoPedidoModule: HistoricoPedidoModule;

  beforeEach(() => {
    historicoPedidoModule = new HistoricoPedidoModule();
  });

  it('should create an instance', () => {
    expect(historicoPedidoModule).toBeTruthy();
  });
});
