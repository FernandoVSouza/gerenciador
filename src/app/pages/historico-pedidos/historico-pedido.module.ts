import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { HistoricoPedidoRoutingModule } from './historico-pedido-routing.module';
import { HistoricoPedidoComponent } from './historico-pedido.component';
import { AppBootstrapModule } from '../../app-bootstrap/app-bootstrap.module';
import { ReactiveFormsModule } from '@angular/forms';
import { NgxPaginationModule } from 'ngx-pagination';

@NgModule({
  imports: [
    CommonModule,
    HistoricoPedidoRoutingModule,
    AppBootstrapModule,
    ReactiveFormsModule,
    NgxPaginationModule
  ],
  declarations: [HistoricoPedidoComponent]
})
export class HistoricoPedidoModule { }