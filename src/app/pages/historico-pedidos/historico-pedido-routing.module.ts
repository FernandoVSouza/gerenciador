import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { HistoricoPedidoComponent } from './historico-pedido.component';

const routes: Routes = [
  {
    path: '',
    component: HistoricoPedidoComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class HistoricoPedidoRoutingModule { }
