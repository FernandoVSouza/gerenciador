import { Component, OnInit, TemplateRef } from '@angular/core';

import { PedidoService } from '../../services/pedido.service';
import { BsModalRef } from 'ngx-bootstrap/modal/bs-modal-ref.service';
import { BsModalService } from 'ngx-bootstrap/modal';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-historico-pedido',
  templateUrl: './historico-pedido.component.html',
  styleUrls: ['./historico-pedido.component.css']
})
export class HistoricoPedidoComponent implements OnInit {
  pedidos: any[];
  modalRef: BsModalRef;
  pedido: any;

  constructor(
    private _pedidoService: PedidoService,
    private _modalService: BsModalService,
    private _toastService: ToastrService
  ) { }

  ngOnInit() {
    try {
      this._getPedidosFromWeb();      
    } catch (error) {
      this._toastService.error('Ops! ' + error);      
    }
  }

  btnDetalhesPedido(pedido: any, modal: TemplateRef<any>): void {
    this.pedido = pedido;
    this.modalRef = this._modalService.show(modal);
  }

  btnAlteraStatusPedido(pedido: any, status: string): void {
    try {
      if(pedido.status_pedido != 'Entregue') {
        this._alteraStatusPedido(pedido.id_pedido, status);        
      } else {
        throw 'Não é possível alterar os status de pedidos já entregues!';
      }
    } catch (error) {
      this._toastService.error('Ops! ' + error);
    }
  }

  private _getPedidosFromWeb(): void {
    this._pedidoService.getPedidosAceitosOuRecusados()
    .subscribe(
      retorno => {
        this.pedidos = retorno;
      },
      error => {
        console.error(error);
        throw 'Erro ao carregar os Pedidos!';
      }
    );
  }

  private _alteraStatusPedido(id_pedido: number, status: string) {
    this._pedidoService.getAlteraStatusPedido(id_pedido, status)
      .subscribe(
        () => {
          this._toastService.success('Status alterado com sucesso!')
          this._getPedidosFromWeb();
        }, error => {
          throw 'Ocorreu um erro ao alterar o Status do Pedido';
        }
      );
  }
}