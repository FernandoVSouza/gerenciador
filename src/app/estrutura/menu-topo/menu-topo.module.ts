import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { AppBootstrapModule } from '../../app-bootstrap/app-bootstrap.module';
import { MenuTopoRoutingModule } from './menu-topo-routing.module';
import { MenuTopoComponent } from './menu-topo.component';

@NgModule({
  imports: [
    CommonModule,
    AppBootstrapModule,
    MenuTopoRoutingModule
  ],
  declarations: [ MenuTopoComponent ]
})
export class MenuTopoModule { }
