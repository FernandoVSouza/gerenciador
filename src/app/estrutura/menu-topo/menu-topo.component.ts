import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

import { AuthService } from '../../services/auth.service';
import { PedidoService } from '../../services/pedido.service';

@Component({
  selector: 'app-menu-topo',
  templateUrl: './menu-topo.component.html',
  styleUrls: ['./menu-topo.component.css']
})
export class MenuTopoComponent implements OnInit {
  menuSideCollapsed: false;
  cadastroCollapsed: false;
  pedidosCollapsed: false;
  relatoriosCollapsed: false;
  qtdePedidos: number;

  constructor(
    private _authService: AuthService,
    private _router: Router,
    private _pedidoService: PedidoService
    ) {}

  ngOnInit() {
    this._getQtdePedidosAguardando();
    setInterval(()=> {
      this._getQtdePedidosAguardando();
    }, 30000);
  }

  logOff(): void {
    this._authService.logOff();
    this._router.navigate(['/login']);
  }

  private _getQtdePedidosAguardando(): void {
    this._pedidoService.getQtdePedidosAguardando()
      .subscribe(
        retorno => this.qtdePedidos = retorno,
        error => console.error(error)
      );
  }

}