import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { MenuTopoComponent } from './menu-topo.component';
import { CanActivateUser } from '../../services/canActivateUser.service';

const routes: Routes = [
  {
    path: '',
    component: MenuTopoComponent,
    children: [
        {
          path: '', 
          redirectTo: 'home', 
          pathMatch: 'prefix',
          canActivate: [CanActivateUser]
        },
        {
          path: 'home', 
          loadChildren: '../../pages/home/home.module#HomeModule',
          canActivate: [CanActivateUser]
        },
        {
          path: 'categoria', 
          loadChildren: '../../pages/cadastros/categoria/categoria.module#CategoriaModule',
          canActivate: [CanActivateUser]
         },
        {
          path: 'ingrediente', 
          loadChildren: '../../pages/cadastros/ingrediente/ingrediente.module#IngredienteModule',
          canActivate: [CanActivateUser]
        },
        {
          path: 'produto', 
          loadChildren: '../../pages/cadastros/produto/produto.module#ProdutoModule',
          canActivate: [CanActivateUser]
        },
        {
          path: 'configuracoes', 
          loadChildren: '../../pages/configuracoes/configuracoes.module#ConfiguracoesModule',
          canActivate: [CanActivateUser]
        },
        {
          path: 'pedidos', 
          loadChildren: '../../pages/pedido/pedido.module#PedidoModule',
          canActivate: [CanActivateUser]
        },
        {
          path: 'historicoPedidos', 
          loadChildren: '../../pages/historico-pedidos/historico-pedido.module#HistoricoPedidoModule',
          canActivate: [CanActivateUser]
        },
        {
          path: 'vendaPorPeriodo', 
          loadChildren: '../../pages/relatorios/venda-por-periodo/venda-por-periodo.module#VendaPorPeriodoModule',
          canActivate: [CanActivateUser]
        },
        {
          path: 'vendaPorProdutoPeriodo', 
          loadChildren: '../../pages/relatorios/venda-produto-periodo/venda-produto-periodo.module#VendaPorProdutoPeriodoModule',
          canActivate: [CanActivateUser]
        },
        {
          path: 'cardapio', 
          loadChildren: '../../pages/relatorios/cardapio/cardapio.module#CardapioModule',
          canActivate: [CanActivateUser]
        },
        {
          path: 'pedidoPorPeriodo', 
          loadChildren: '../../pages/relatorios/pedido-por-periodo/pedido-por-periodo.module#PedidoPorPeriodoModule',
          canActivate: [CanActivateUser]
        },
        {
          path: 'perfil', 
          loadChildren: '../../pages/perfil/perfil.module#PerfilModule',
          canActivate: [CanActivateUser]
        },
    ],
    canActivate: [CanActivateUser]
  }
];
@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class MenuTopoRoutingModule { }
