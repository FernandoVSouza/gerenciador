import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AppComponent } from './app.component';
import { CanActivateUser } from './services/canActivateUser.service';

const routes: Routes = [
  { 
    path: '', 
    loadChildren: './estrutura/menu-topo/menu-topo.module#MenuTopoModule',
    canActivate: [CanActivateUser]
  },
  { 
    path: 'login', 
    loadChildren: './pages/login/login.module#LoginModule'
  },
  {
    path: '',
    redirectTo: '',
    pathMatch: 'full'
  }
];

@NgModule({
  imports: [ RouterModule.forRoot(routes) ],
  exports: [ RouterModule ],
})

export class AppRoutingModule {}
