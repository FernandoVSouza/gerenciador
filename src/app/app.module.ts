import { BrowserModule } from '@angular/platform-browser';
import { LOCALE_ID } from '@angular/core';
import { registerLocaleData } from '@angular/common';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { NgModule } from '@angular/core';
import { ToastrModule } from 'ngx-toastr';
import { HttpClientModule } from '@angular/common/http';
import { FormsModule } from '@angular/forms';
import localePt from '@angular/common/locales/pt';

import { AppComponent } from './app.component';

import { AppRoutingModule } from './app-routing.module';

import { CategoriaService } from './services/categoria.service';
import { IngredienteService } from './services/ingrediente.service';
import { ProdutoService } from './services/produto.service';
import { AuthService } from './services/auth.service';
import { CanActivateUser } from './services/canActivateUser.service';
import { ConfiguracaoService } from './services/configuracao.service';
import { PedidoService } from './services/pedido.service';
import { DashboardService } from './services/dashboard.service';
import { RelatorioService } from './services/relatorios.service';

registerLocaleData(localePt, 'pt-BR');

@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    AppRoutingModule,
    FormsModule,
    HttpClientModule,
    ToastrModule.forRoot({
      positionClass: 'toast-bottom-right'
    }),
  ],
  providers: [
    { provide: LOCALE_ID, useValue: 'pt-BR' },
    AuthService,
    CategoriaService,
    IngredienteService,
    ProdutoService,
    CanActivateUser,
    ConfiguracaoService,
    PedidoService,
    DashboardService,
    RelatorioService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
