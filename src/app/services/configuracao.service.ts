import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';
import { IUserAutenticated } from '../models/auth.model';
import { IConfiguracao } from '../models/configuracao.model';

@Injectable()
export class ConfiguracaoService {
    private readonly _URL: string = 'http://localhost:8000/api/';

    constructor(
        private _http: HttpClient
    ) {}

    private getToken(): IUserAutenticated {
        return JSON.parse(localStorage.getItem('user'));
    }

    private getHeaders(): any {
        const { token_type, access_token } = this.getToken();
        const httpOptions = {
            headers: new HttpHeaders({
                "Authorization": `${token_type} ${access_token}`,
                "Content-Type":  "application/application/json; charset=utf-8",
                "Accept": 'application/json'
            })
        };

        return httpOptions;
    }

    cadastrarConfiguracao(configuracao: any): Observable<any> {
        return this._http.post(`${this._URL}configuracao`, configuracao, this.getHeaders());
    }

    getConfiguracao(): Observable<any> {
        return this._http.get(`${this._URL}configuracao`, this.getHeaders());
    }

    saveConfiguracao(configuracao: IConfiguracao): Observable<any> {
        return this._http.post(`${this._URL}configuracao`, configuracao, this.getHeaders());
    }

    updateConfiguracao(configuracao: IConfiguracao): Observable<any> {
        return this._http.put(this._URL + 'configuracao/' + configuracao.id_configuracao, configuracao, this.getHeaders());
    }
}