import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';
import { ICategoria } from '../models/categoria.model';
import { IUserAutenticated } from '../models/auth.model';

@Injectable()
export class CategoriaService {
    private readonly _URL: string = 'http://localhost:8000/api/';
    // private readonly _URL: string = 'http://fernandovicente.alphi.media/api/';
    private _headers = new HttpHeaders();

    constructor(
        private _http: HttpClient
    ) {}

    private getToken(): IUserAutenticated {
        return JSON.parse(localStorage.getItem('user'));
    }

    private getHeaders(): any {
        const { token_type, access_token } = this.getToken();
        const httpOptions = {
            headers: new HttpHeaders({
                "Authorization": `${token_type} ${access_token}`,
                "Content-Type":  "application/application/json; charset=utf-8",
                "Accept": 'application/json'
            })
        };

        return httpOptions;
    }

    cadastrarCategoria(categoria: any): Observable<any> {
        return this._http.post(`${this._URL}categoria`, categoria, this.getHeaders());
    }

    getCategorias(): Observable<any> {
        return this._http.get(`${this._URL}categoria`, this.getHeaders());
    }

    ativarCategoria(categoria: ICategoria): Promise<any> {
        return this._http.get(this._URL + 'categoria/ativar/' + categoria.id_categoria, this.getHeaders()).toPromise();
    }

    desativarCategoria(categoria: ICategoria): Promise<any> {
        return this._http.get(this._URL + 'categoria/desativar/' + categoria.id_categoria, this.getHeaders()).toPromise();
    }

    updateCategoria(categoria: ICategoria): Observable<any> {
        return this._http.put(this._URL + 'categoria/' + categoria.id_categoria, categoria, this.getHeaders());
    }
}