import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';

import { IProduto } from '../models/produto.model';
import { IUserAutenticated } from '../models/auth.model';

@Injectable()
export class ProdutoService {
    private readonly _URL: string = 'http://localhost:8000/api/';

    constructor(
        private _http: HttpClient
    ) {}

    private getToken(): IUserAutenticated {
        return JSON.parse(localStorage.getItem('user'));
    }

    private getHeaders(): any {
        const { token_type, access_token } = this.getToken();
        const httpOptions = {
            headers: new HttpHeaders({
                "Authorization": `${token_type} ${access_token}`,
                "Content-Type":  "application/application/json; charset=utf-8",
                "Accept": 'application/json'
            })
        };

        return httpOptions;
    }

    cadastrarProduto(produto: IProduto): Observable<any> {
        return this._http.post(this._URL + 'produto', produto, this.getHeaders());
    }

    getProdutos(): Observable<any> {
        return this._http.get(`${this._URL}produto`, this.getHeaders());
    }

    ativarProduto(produto: IProduto): Promise<any> {
        return this._http.get(this._URL + 'produto/ativar/' + produto.id_produto, this.getHeaders()).toPromise();
    }

    desativarProduto(produto: IProduto): Promise<any>{
        return this._http.get(this._URL + 'produto/desativar/' + produto.id_produto, this.getHeaders()).toPromise();
    }
    
    alterDestaque(id_produto: number): Promise<any>{
        return this._http.get(this._URL + 'produto/alterDestaque/' + id_produto, this.getHeaders()).toPromise();
    }

    updateProduto(produto: IProduto): Observable<any> {
        return this._http.put(this._URL + 'produto/' + produto.id_produto, produto, this.getHeaders());
    }
}