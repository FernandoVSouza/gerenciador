import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';

import { IAuth, IUserAutenticated } from '../models/auth.model';

@Injectable()
export class AuthService {
    private readonly _URL: string = 'http://localhost:8000/api';

    constructor(
        private _http: HttpClient
    ) {}

    autenticateUser({email, password}: IAuth): Observable<IUserAutenticated> {
        return this._http.post<IUserAutenticated>(`${this._URL}/login`, { email, password });
    }

    updateUser(user: any, id: number): Observable<any> {
        return this._http.put<any>(`${this._URL}/updateUser/${id}`, user);
    }

    saveUserAutenticated(user: IUserAutenticated): void {
        localStorage.setItem('user', JSON.stringify(user));
    }

    getUserAutenticated(): IUserAutenticated {
        const user: IUserAutenticated = JSON.parse(localStorage.getItem('user'));
        if(user) {
            const data_atual: Date = new Date();
            const data_expires: Date = new Date(user.expires_at);
            return data_atual < data_expires ? user : null;
        } else {
            return null;
        }
    }

    isLoggedIn(): boolean {
        const user: IUserAutenticated = JSON.parse(localStorage.getItem('user'));
        if(user) {
            const data_atual: Date = new Date();
            const data_expires: Date = new Date(user.expires_at);
            return data_atual < data_expires ? true : false;
        } else {
            return false;
        }
    }

    logOff(): void {
        localStorage.removeItem('user');
    }
}
