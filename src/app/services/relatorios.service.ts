import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';

import { IUserAutenticated } from '../models/auth.model';

@Injectable()
export class RelatorioService {
    private readonly _URL: string = 'http://localhost:8000/api';

    constructor(
        private _http: HttpClient
    ) {}

    private getToken(): IUserAutenticated {
        return JSON.parse(localStorage.getItem('user'));
    }

    private getHeaders(): any {
        const { token_type, access_token } = this.getToken();
        const httpOptions = {
            headers: new HttpHeaders({
                "Authorization": `${token_type} ${access_token}`,
                "Content-Type":  "application/application/json; charset=utf-8",
                "Accept": 'application/json'
            })
        };

        return httpOptions;
    }

    getRelatorioVendaPorPeriodo(dataInicial: Date, dataFinal: Date): Observable<any> {
        return this._http.get(`${this._URL}/relatorios/VendaPorPeriodo/${dataInicial}/${dataFinal}`, this.getHeaders());
    }

    getRelatorioPedidoPorPeriodo(dataInicial, dataFinal): Observable<any> {
        return this._http.get(`${this._URL}/relatorios/PedidoPorPeriodo/${dataInicial}/${dataFinal}`, this.getHeaders());
    }

    getRelatorioVendaDeProdutoPorPeriodo(dataInicial: Date, dataFinal: Date, fk_categoria: number, quantidade: number): Observable<any> {
        return this._http.get(`${this._URL}/relatorios/VendaDeProdutoPorPeriodo/${dataInicial}/${dataFinal}/${fk_categoria}/${quantidade}`, this.getHeaders());
    }

    getCardapio(): Observable<any> {
        return this._http.get(`${this._URL}/relatorios/cardapio`, this.getHeaders());
    }
}
