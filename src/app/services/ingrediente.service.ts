import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';

import { IIngrediente } from '../models/ingrediente.model';
import { IUserAutenticated } from '../models/auth.model';

@Injectable()
export class IngredienteService {
    private readonly _URL: string = 'http://localhost:8000/api/';
    private _headers = new HttpHeaders();

    constructor(
        private _http: HttpClient
    ) {}

    private getToken(): IUserAutenticated {
        return JSON.parse(localStorage.getItem('user'));
    }

    private getHeaders(): any {
        const { token_type, access_token } = this.getToken();
        const httpOptions = {
            headers: new HttpHeaders({
                "Authorization": `${token_type} ${access_token}`,
                "Content-Type":  "application/application/json; charset=utf-8",
                "Accept": 'application/json'
            })
        };

        return httpOptions;
    }

    cadastrarIngrediente(ingrediente: IIngrediente): Observable<any> {
        return this._http.post(this._URL + 'ingrediente', ingrediente, this.getHeaders());
    }

    getIngredientes(): Observable<any> {
        return this._http.get(`${this._URL}ingrediente`, this.getHeaders());
    }

    ativarIngrediente(ingrediente: IIngrediente): Promise<any> {
        return this._http.get(this._URL + 'ingrediente/ativar/' + ingrediente.id_ingrediente, this.getHeaders()).toPromise();
    }

    desativarIngrediente(ingrediente: IIngrediente): Promise<any>{
        return this._http.get(this._URL + 'ingrediente/desativar/' + ingrediente.id_ingrediente, this.getHeaders()).toPromise();
    }

    updateIngrediente(ingrediente: IIngrediente): Observable<any> {
        return this._http.put(this._URL + 'ingrediente/' + ingrediente.id_ingrediente, ingrediente, this.getHeaders());
    }
}