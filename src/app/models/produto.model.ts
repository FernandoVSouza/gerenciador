import { IIngrediente } from "./ingrediente.model";

export interface IProduto{
    id_produto?: number;
    descricao: string;
    fk_categoria: number;
    preco_unitario: number;
    ingredientes: number[];
    ativo?: boolean;
}

export interface IProdutoModel{
    id_produto: number;
    descricao: string;
    fk_categoria: number;
    categoria: string;
    preco_unitario: number;
    ingredientes: number[];
    ativo: boolean;
    destaque: boolean;
}