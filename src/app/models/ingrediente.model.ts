export interface IIngrediente{
    id_ingrediente?: number;
    descricao: string;
    preco: number;
    incremento: boolean;
    ativo?: boolean;
}

export class IngredienteProduto implements IIngrediente {
    checked: boolean;
    id_ingrediente?: number;
    descricao: string;
    preco: number;
    incremento: boolean;
    ativo?: boolean;
}