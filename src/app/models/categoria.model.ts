export interface ICategoria{
    id_categoria?: number,
    descricao: string,
    alteracao: boolean,
    incremento: boolean,
    ordem: number,
    ativo?: boolean,
}