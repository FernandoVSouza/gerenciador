export interface IConfiguracao{
    id_configuracao?: number;
    status: number;
    latitude: number;
    longitude: number;
    distancia_sem_taxa: number;
    distancia_com_taxa: number;
    preco_entrega: number;
    ativo?: boolean;
}