export interface IAuth {
    email: string;
    password: string;
}

export interface IUserAutenticated {
    access_token: string;
    expires_at: string;
    token_type: string;
    user: IUser;
}

export interface IUser {
    id: number;
    name: string;
    email: string;
    phone?: string;
    password?: string;
    cpassword?: string;
}